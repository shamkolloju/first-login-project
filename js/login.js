// 1. when we click on login button the form should be validated
// 2. if we have any errors it should show the error msg
// 3. if we have no errors the form should be submitted


// implementation
// 1. we have to create a reference object to the login button
// 2. we should define the function which perform validation
// 3. we should set onclick eventhandler property to the var and it should be equal to the function.



function validate() {
    var email = document.getElementById("email-address").value;
    var password = document.getElementById("password").value;
    var emailreg = /^[a-zA-Z]+([_\.-]?[a-zA-Z0-9]+)*@[a-zA-Z0-9]+([\.-]?[a-zA-Z0-9]+)*(\.[a-zA-Z]{2,4})+$/;
    var passwd = /^[a-zA-Z0-9!@#$%^&*]{6,50}$/;
    if (email != "" && password != "") {
        if (email.match(emailreg)) {
            if (password.match(passwd)) {
                alert("validation successfull")
                return true;
            }
            else {
                alert("enter a valid password");
                return false;
            }
        }
        else {
            alert("enter a valid email address");
            return false;
        }
    }
    else {
        alert("fillout the fields");
        return false;
    }
}
